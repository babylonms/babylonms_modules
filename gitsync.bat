@echo off
echo Pull across all modules if you are granted.
echo Press any key for PULL or CTRL+C
pause
git pull

set p0=".\bms_imagebuffer"
echo ----------%p0%
git -C %p0% pull

set p0=".\bms_inputcontroller"
echo ----------%p0%
git -C %p0% pull

set p0=".\bms_windowslist"
echo ----------%p0%
git -C %p0% pull

set p0=".\bms_screencontentexporter"
echo ----------%p0%
git -C %p0% pull

set p0=".\bms_screenshotclient"
echo ----------%p0%
git -C %p0% pull

echo Finished
